<?php
App::uses('Adverb', 'Model');

/**
 * Adverb Test Case
 *
 */
class AdverbTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.adverb'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Adverb = ClassRegistry::init('Adverb');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Adverb);

		parent::tearDown();
	}

}

<?php
App::uses('ClassDay', 'Model');

/**
 * ClassDay Test Case
 *
 */
class ClassDayTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.class_day'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ClassDay = ClassRegistry::init('ClassDay');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ClassDay);

		parent::tearDown();
	}

}

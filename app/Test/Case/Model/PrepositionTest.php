<?php
App::uses('Preposition', 'Model');

/**
 * Preposition Test Case
 *
 */
class PrepositionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.preposition'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Preposition = ClassRegistry::init('Preposition');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Preposition);

		parent::tearDown();
	}

}

<?php
App::uses('SimpleWord', 'Model');

/**
 * SimpleWord Test Case
 *
 */
class SimpleWordTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.simple_word'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SimpleWord = ClassRegistry::init('SimpleWord');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SimpleWord);

		parent::tearDown();
	}

}

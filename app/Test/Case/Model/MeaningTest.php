<?php
App::uses('Meaning', 'Model');

/**
 * Meaning Test Case
 *
 */
class MeaningTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.meaning',
		'app.word'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Meaning = ClassRegistry::init('Meaning');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Meaning);

		parent::tearDown();
	}

}

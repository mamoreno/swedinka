<?php
App::uses('WordsController', 'Controller');

/**
 * WordsController Test Case
 *
 */
class WordsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.word',
		'app.image',
		'app.meaning',
		'app.tag',
		'app.tags_word'
	);

}

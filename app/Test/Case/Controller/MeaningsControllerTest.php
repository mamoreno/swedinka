<?php
App::uses('MeaningsController', 'Controller');

/**
 * MeaningsController Test Case
 *
 */
class MeaningsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.meaning',
		'app.word',
		'app.image',
		'app.tag',
		'app.tags_word'
	);

}

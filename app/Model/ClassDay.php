<?php
App::uses('AppModel', 'Model');
/**
 * ClassDay Model
 *
 */
class ClassDay extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'date';
	public $validate = array(
		'date' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'La fecha no puede estar vacía',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);
}

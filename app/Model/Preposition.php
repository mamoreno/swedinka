<?php
App::uses('AppModel', 'Model');
/**
 * Preposition Model
 *
 */
class Preposition extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'preposition';

}

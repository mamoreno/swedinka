<?php
App::uses('AppModel', 'Model');
/**
 * Adverb Model
 *
 */
class Adverb extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'adverb';

}

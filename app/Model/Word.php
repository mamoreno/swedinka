<?php
App::uses('AppModel', 'Model');
/**
 * Word Model
 *
 * @property Image $Image
 * @property Meaning $Meaning
 * @property Tag $Tag
 */
class Word extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'word';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'genre' => array(
			'inlist' => array(
              'rule' => array('inList', array('en', 'ett', 'En', 'Ett')),
              	'message' => 'El género no vale',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'El género no puede estar vacío',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'word' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'La palabra no puede estar vacía',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'plural' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Tiene que tener plural',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'articled' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'No has completado la palabra articulada',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'articled_plural' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'No has completado la palabra en plural articulada',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Image' => array(
			'className' => 'Image',
			'foreignKey' => 'word_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Meaning' => array(
			'className' => 'Meaning',
			'foreignKey' => 'word_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Tag' => array(
			'className' => 'Tag',
			'joinTable' => 'tags_words',
			'foreignKey' => 'word_id',
			'associationForeignKey' => 'tag_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}

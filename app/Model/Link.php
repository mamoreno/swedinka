<?php
App::uses('AppModel', 'Model');
/**
 * Link Model
 *
 */
class Link extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';
	public $validate = array(
			'url' => array(
				'url' => array(
					'rule' => array('url'),
					'message' => 'No es un link',
					'allowEmpty' => false,
					'required' => true,
					//'last' => false, // Stop validation after this rule
					//'on' => 'create', // Limit validation to 'create' or 'update' operations
				),
			),
			'title' => array(
				'notempty' => array(
					'rule' => array('notempty'),
					'message' => 'El título no puede estar vacío',
					'allowEmpty' => false,
					'required' => true,
					//'last' => false, // Stop validation after this rule
					//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),	
		);
}

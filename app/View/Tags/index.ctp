<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Palabras'));
	?>
	<div data-role="content">
		<ul data-role="listview" data-inset="true" data-filter="true">
			<li data-role="list-divider">Etiquetas</li>

			<?php foreach ($tags as $tag):
				$t=$tag['Tag'];
			?>
			<li>
				<?php
					echo $this->Html->link(  $this->Util->uppFirst( $t['tag'] )
				 	, array('controller'=>'words', 'action'=>'listarPorTag', $t['id']));
				 ?>
			</li>
			<?php endforeach;?>
		</ul>
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->


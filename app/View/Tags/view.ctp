<div class="tags view">
<h2><?php  echo __('Tag'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($tag['Tag']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tag'); ?></dt>
		<dd>
			<?php echo h($tag['Tag']['tag']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tag'), array('action' => 'edit', $tag['Tag']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tag'), array('action' => 'delete', $tag['Tag']['id']), null, __('Are you sure you want to delete # %s?', $tag['Tag']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tags'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tag'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Words'), array('controller' => 'words', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Word'), array('controller' => 'words', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Words'); ?></h3>
	<?php if (!empty($tag['Word'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Genre'); ?></th>
		<th><?php echo __('Word'); ?></th>
		<th><?php echo __('Plural'); ?></th>
		<th><?php echo __('Articled'); ?></th>
		<th><?php echo __('Articled Plural'); ?></th>
		<th><?php echo __('Comments'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($tag['Word'] as $word): ?>
		<tr>
			<td><?php echo $word['id']; ?></td>
			<td><?php echo $word['genre']; ?></td>
			<td><?php echo $word['word']; ?></td>
			<td><?php echo $word['plural']; ?></td>
			<td><?php echo $word['articled']; ?></td>
			<td><?php echo $word['articled_plural']; ?></td>
			<td><?php echo $word['comments']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'words', 'action' => 'view', $word['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'words', 'action' => 'edit', $word['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'words', 'action' => 'delete', $word['id']), null, __('Are you sure you want to delete # %s?', $word['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Word'), array('controller' => 'words', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

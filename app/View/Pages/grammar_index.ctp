<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Gramática básica'));
	?>
	<div data-role="content">
		<ul data-role="listview" data-inset="true" data-filter="false">
 			<li data-role="list-divider">Gram&aacute;tica b&aacute;sica</li>
 			<li>
 				<?php echo $this->Html->link('¿Cómo se forman los plurales?', 
 				array('controller'=>'pages', 'action'=>'display', 'plurales')); ?>
			</li>
		</ul>			
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->


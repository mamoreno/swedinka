<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Días de la semana'));
	?>
	<div data-role="content">
		<ul data-role="listview" data-inset="true" data-filter="false">
			<li data-role="list-divider">Los 7 d&iacute;as de la semana</li>
			<li>
				M&aring;ndag<span class="ui-li-aside">Lunes</span>
			</li>
			<li>
				Tisdag<span class="ui-li-aside">Martes</span>
			</li>
			<li>
				Onsdag<span class="ui-li-aside">Mi&eacute;rcoles</span>
			</li>
			<li>
				Tursdag<span class="ui-li-aside">Jueves</span>
			</li>
			<li>
				Fredag<span class="ui-li-aside">Viernes</span>
			</li>
			<li>
				L&ouml;rdag<span class="ui-li-aside">S&aacute;bado</span>
			</li>
			<li>
				S&ouml;ndag<span class="ui-li-aside">Domingo</span>	
			</li>
		</ul>			
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->


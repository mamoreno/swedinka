<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Estaciones'));
	?>
	<div data-role="content">
		<ul data-role="listview" data-inset="true" data-filter="false">
			<li data-role="list-divider">Los 4 estaciones</li>
			<li>
				Vinter<span class="ui-li-aside">Invierno</span>
			</li>
			<li>
				V&aring;r<span class="ui-li-aside">Primavera</span>
			</li>
			<li>
				Sommar<span class="ui-li-aside">Verano</span>
			</li>
			<li>
				H&ouml;st<span class="ui-li-aside">Oto&ntilde;o</span>
			</li>
		</ul>			
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->


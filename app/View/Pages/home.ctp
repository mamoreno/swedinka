<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Palabras', 'hideBackButton'=>1));
	?>
	<div data-role="content">
		<ul data-role="listview" data-inset="true" data-filter="false">
			<li data-role="list-divider">Listados de palabras</li>

			<li>			
				<a href="<?php echo $this->Html->url(array('controller'=>'words', 'action'=>'listar'));?>">
					<h2>Sustantivos por orden alfab&eacute;tico</h2>
					<p>Listado con palabras, sus plurales, g&eacute;nero..</p>
				</a>
			</li>
			<li>			
				<a href="<?php echo $this->Html->url(array('controller'=>'simple_words', 'action'=>'index'));?>">
					<h2>Palabras "sueltas"</h2>
					<p>Listado con palabras que no tienen plural, art&iacute;culo... (Jag, April, nog...)</p>
				</a>
			</li>
			<li>			
				<a href="<?php echo $this->Html->url(array('controller'=>'prepositions', 'action'=>'index'));?>">
					<h2>Preposiciones</h2>
					<p>i, p&aring;, mellan... Con ejemplos</p>
				</a>
			</li>			
			<li>			
				<a href="<?php echo $this->Html->url(array('controller'=>'tags', 'action'=>'index'));?>">
					<h2>Listas por etiquetas</h2>
					<p></p>
				</a>
			</li>				
			<li>
				<?php echo $this->Html->link('Lista de En-Palabras', 
				array('controller'=>'words', 'action'=>'en')); ?>
			</li>
			<li>
				<?php echo $this->Html->link('Lista de Ett-Palabras', 
				array('controller'=>'words', 'action'=>'ett')); ?>
			</li>
			<li>
 				<?php echo $this->Html->link('Días de la semana', 
 				array('controller'=>'pages', 'action'=>'display', 'dias_semana')); ?>
			</li>	
			<li>
 				<?php echo $this->Html->link('Estaciones', 
 				array('controller'=>'pages', 'action'=>'display', 'estaciones')); ?>
			</li>
 			<li>
 				<?php echo $this->Html->link('Meses', 
 				array('controller'=>'pages', 'action'=>'display', 'meses')); ?>
			</li>		
			<li data-role="list-divider">Gram&aacute;tica b&aacute;sica</li>
 			<li>
 				<?php echo $this->Html->link('Gramática Básica', 
 				array('controller'=>'pages', 'action'=>'display', 'grammar_index')); ?>
			</li>	
			<li data-role="list-divider">Juegos</li>
			<li>
 				<?php echo $this->Html->link('En.. eller ett?', 
 				array('controller'=>'words', 'action'=>'enEllerEtt')); ?>
			</li>	
 			<li>
 				<?php echo $this->Html->link('Adivina el plural', 
 				array('controller'=>'words', 'action'=>'guessPlural')); ?>
			</li>	
			<li data-role="list-divider">Otros</li>
 			<li>
 				<?php echo $this->Html->link('Links interesantes', 
 				array('controller'=>'links', 'action'=>'index')); ?>
			</li>	
 			<li>
 				<?php echo $this->Html->link('¿Qué se ha hecho en clase el día...?', 
 				array('controller'=>'class_days', 'action'=>'index')); ?>
			</li>	
		</ul>			
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->



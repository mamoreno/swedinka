<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Meses del año'));
	?>
	<div data-role="content">
		<ul data-role="listview" data-inset="true" data-filter="false">
			<li data-role="list-divider">Los 12 meses del a&ntilde;o</li>
			<li>
				Januari<span class="ui-li-aside">Enero</span>
			</li>
			<li>
				Februari<span class="ui-li-aside">Febrero</span>
			</li>
			<li>
				Mars<span class="ui-li-aside">Marzo</span>
			</li>
			<li>
				April<span class="ui-li-aside">Abril</span>
			</li>
			<li>
				Maj<span class="ui-li-aside">Mayo</span>
			</li>
			<li>
				Juni<span class="ui-li-aside">Junio</span>
			</li>
			<li>
				Juli<span class="ui-li-aside">Julio</span>
			</li>
			<li>
				Augusti<span class="ui-li-aside">Agosto</span>	
			</li>
			<li>
				September<span class="ui-li-aside">Septiembre</span>	
			</li>
			<li>
				Oktober<span class="ui-li-aside">Octubre</span>	
			</li>
			<li>
				November<span class="ui-li-aside">Noviembre</span>	
			</li>
			<li>
				December<span class="ui-li-aside">Diciembre</span>	
			</li>
		</ul>			
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->


<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Palabras'));
	?>
	<div data-role="content">
		¿C&oacute;mo formar el plural?
		<div data-role="collapsible-set">
			<div data-role="collapsible" data-collapsed="false" data-theme="b" data-content-theme="a">
				<h2>En-Palabras</h2>
				<p>
					<div data-role="collapsible-set">
						<div data-role="collapsible" data-theme="b" data-content-theme="c">
							<h2>1. -A &rarr; -OR</h2>
							<p>
							Todas las en-palabras que terminan en <span class="destacado">"-A"</span> forman su plural sustituyendo la <span class="destacado">"-A"</span> final por <span class="destacado">"-OR"</span>.
							</p>
							<p>Ejemplos:</p>
							<ul data-role="listview" data-inset="true">
									<li>Kvinn<span class="destacado">a</span>
										<span class="ui-li-aside">Kvinn<span class="destacado">or</span></span>
									</li>
									<li>Kron<span class="destacado">a</span>
										<span class="ui-li-aside">Kron<span class="destacado">or</span></span>
									</li>
									<li>Flick<span class="destacado">a</span>
										<span class="ui-li-aside">Flick<span class="destacado">or</span></span>
									</li>
							</ul>
						</div>

						<div data-role="collapsible" data-theme="b" data-content-theme="c">
							<h2>2. -E &rarr; -AR</h2>
							<p>
								A- Muchas en-palabras que tienen una &uacute;nica s&iacute;laba, a&ntilde;aden <span class="destacado">"-AR"</span> al final.
							</p>
							<p>Ejemplos:</p>
							<ul data-role="listview" data-inset="true">			
								<li>Dag<span class="destacado"></span>
									<span class="ui-li-aside">Dag<span class="destacado">ar</span></span>
								</li>
								<li>Stol<span class="destacado"></span>
									<span class="ui-li-aside">Stol<span class="destacado">ar</span></span>
								</li>

							</ul>
							<br/>
							<p>
							B- Muchas en-palabras que tienen 2 vocales y acaban en <span class="destacado">"-E"</span>, <span class="destacado">"-EL"</span>, <span class="destacado">"-ER"</span>, <span class="destacado">"-EN"</span> forman su plural con la terminaci&oacute;n <span class="destacado">"-AR"</span>:
							</p>
							<p>Por ejemplo:</p>
							<ul data-role="listview" data-inset="true">
									<li>Pojk<span class="destacado">e</span>
										<span class="ui-li-aside">Pojk<span class="destacado">ar</span></span>
									</li>
									<li>Timm<span class="destacado">e</span>
										<span class="ui-li-aside">Timm<span class="destacado">ar</span></span>
									</li>
									<li>Bull<span class="destacado">e</span>
										<span class="ui-li-aside">Bull<span class="destacado">ar</span></span>
									</li>
									<li>Ax<span class="destacado">el</span>
										<span class="ui-li-aside">Axl<span class="destacado">ar</span></span>
									</li>
									<li>Syst<span class="destacado">er</span>
										<span class="ui-li-aside">Systr<span class="destacado">ar</span></span>
									</li>
							</ul>
							<br/>
							<p>
								C- Las en-palabras que terminan en <span class="destacado">"-ING"</span> a&ntilde;aden <span class="destacado">"-AR"</span> al final.
							</p>
							<p>Ejemplos:</p>
							<ul data-role="listview" data-inset="true">
								<li>Tidn<span class="destacado">ing</span>
									<span class="ui-li-aside">Tidning<span class="destacado">ar</span></span>
								</li>
								<li>Kyckl<span class="destacado">ing</span>
									<span class="ui-li-aside">Kyckling<span class="destacado">ar</span></span>
								</li>
							</ul>
						</div>

						<div data-role="collapsible" data-theme="b" data-content-theme="c">
							<h2>3. -&Oslash; &rarr; -ER</h2>
							<p>
								A- Aquellas en-palabras que tienen el acento (golpe de voz) en la &uacute;ltima s&iacute;laba, a&ntilde;aden <span class="destacado">"-ER"</span> al final.
							</p>
							<ul data-role="listview" data-inset="true">
								<li>Apel<span class="acentuado">sin</span>
									<span class="ui-li-aside">Apel<span class="acentuado">sin</span><span class="destacado">er</span></span>
								</li>					
								<li>Ba<span class="acentuado">nan</span>
									<span class="ui-li-aside">Ba<span class="acentuado">nan</span><span class="destacado">er</span></span>
								</li>					
								<li>To<span class="acentuado">mat</span>
									<span class="ui-li-aside">To<span class="acentuado">mat</span><span class="destacado">er</span></span>
								</li>					
							</ul>
							<br/>
							<p>
								B- Muchas en-palabras que tienen una &uacute;nica s&iacute;laba, a&ntilde;aden <span class="destacado">"-ER"</span> al final. Casi todas las palabras de origen extranjero a&ntilde;aden <span class="destacado">"-ER"</span>.
							</p>
							<ul data-role="listview" data-inset="true">
								<li>Text<span class="destacado"></span>
									<span class="ui-li-aside">Text<span class="destacado">er</span></span>
								</li>
								<li>Film<span class="destacado"></span>
									<span class="ui-li-aside">Film<span class="destacado">er</span></span>
								</li>
								<li>Telefon<span class="destacado"></span>
									<span class="ui-li-aside">Telefon<span class="destacado">er</span></span>
								</li>
							</ul>
							<br/>
							<p>
								C- Algunas palabras cambian la vocal <span class="destacado">"-A"</span> por <span class="destacado">"-&Auml;"</span> y <span class="destacado">"O"</span> por <span class="destacado">"Ö"</span> en el plural y forman su plural a&ntilde;adiendo <span class="destacado">"-ER"</span>.
							</p>
							<ul data-role="listview" data-inset="true">
								<li>Fot
									<span class="ui-li-aside">F<span class="acentuado">ö</span>tt<span class="destacado">er</span></span>
								</li>
								<li>Son
									<span class="ui-li-aside">S<span class="acentuado">ö</span>n<span class="destacado">er</span></span>
								</li>
							</ul>
						</div>
					</div>
				</p>
			</div>
			<div data-role="collapsible" data-collapsed="true" data-theme="b" data-content-theme="a">
				<h2>Ett-Palabras</h2>
				<p>
					<div data-role="collapsible-set">
						<div data-role="collapsible" data-theme="b" data-content-theme="c">
							<h2>4. -vokal &rarr; -N</h2>
							<p>
							Las ett-palabras que terminan en <span class="destacado">"vocal"</span> forman su plural a&ntilde;adiendo <span class="destacado">"-N"</span> al final.
							</p>
							<p>Ejemplos:</p>
							<ul data-role="listview" data-inset="true">
								<li>&Auml;ppl<span class="destacado">e</span>
									<span class="ui-li-aside">&Auml;ppl<span class="destacado">e</span><span class="destacado">n</span></span>
								</li>
									
							</ul>
						</div>
						<div data-role="collapsible" data-theme="b" data-content-theme="c">
							<h2>5. -&Oslash; &rarr; -&Oslash;</h2>
							<p>
							Todas las ett-palabras que terminan en <span class="destacado">"consonante" NO a&ntilde;aden NADA</span> en el plural.
							</p>
							<p>Ejemplos:</p>
							<ul data-role="listview" data-inset="true">
								<li>Hus
									<span class="ui-li-aside">Hus</span>
								</li>
								<li>Barn
									<span class="ui-li-aside">Barn</span>
								</li>
							</ul>
						</div>
						<div data-role="collapsible" data-theme="b" data-content-theme="c">
							<h2>6. -&Eacute; &rarr; -ER</h2>
							<p>
							Las ett-palabras que terminan con vocal acentuada (con tilde) forman su plural a&ntilde;adiendo <span class="destacado">"-ER"</span> final por.
							</p>
							<p>Ejemplos:</p>
							<ul data-role="listview" data-inset="true">
								<li>Caf<span class="destacado">&eacute;</span>
									<span class="ui-li-aside">Caf<span class="acentuado">&eacute;</span><span class="destacado">er</span></span>
								</li>
							</ul>
						</div>
					</div>
				</p>
			</div>
			<div data-role="collapsible" data-collapsed="true" data-theme="b" data-content-theme="a">
				<h2>Otros...</h2>
				<p>
					<div data-role="collapsible-set">
						<div data-role="collapsible" data-theme="b" data-content-theme="c">
							<h2>7. -ARE &rarr; -ARE</h2>
							<p>
							Todas las en-palabras que terminan en <span class="destacado">"-ARE" NO a&ntilde;aden NADA</span> en el plural.
							</p>
							<p>Ejemplos:</p>
							<ul data-role="listview" data-inset="true">
								<li>L&auml;r<span class="destacado">are</span>
									<span class="ui-li-aside">L&auml;r<span class="destacado">are</span></span>
								</li>
									
							</ul>
						</div>
				</p>
			</div>
	</div>

	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->

<style type="text/css">
.destacado{
	color: red;
}
.acentuado{
	color: green;
}
</style>


<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Palabras'));
	?>
	<div data-role="content">
		<ul data-role="listview" data-inset="true" data-filter="true">
			<li data-role="list-divider">¿Qu&eacute; hemos hecho cada d&iacute;a?</li>

			<?php foreach ($classDays as $cd):
				$cd=$cd['ClassDay'];
			?>
			<li>
				<?php
					echo $this->Html->link( $this->Fechas->spanish( $cd['date'] )
				 	, array('controller'=>'class_days', 'action'=>'view', $cd['id']));
				 ?>
			</li>
			<?php endforeach;?>
		<!-- Si hay usuario logueado muestro las opciones -->
		<a href="<?php echo $this->Html->url(array('controller'=>'class_days', 'action'=>'add'));?>" 
			data-icon="plus" data-iconpos="left" data-inset="true" 
			data-role="button" data-theme="e">Nuevo d&iacute;a de clase</a>
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->

<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Palabras'));
	?>
	<div data-role="content">
		<center><p>
			Hecho el d&iacute;a <?php echo $this->Fechas->spanish( $classDay['ClassDay']['date']); ?>
		</p></center>

		<ul data-role="listview" data-inset="true" data-filter="false">

			<li class="centrado" data-role="list-divider">
				Del libro amarillo (Teor&iacute;a)
			</li>
			<li class="centrado">
				<?php echo $classDay['ClassDay']['teoria']; ?>
			</li>
			<li class="centrado" data-role="list-divider">
				Del libro azul (Ejercicios)
			</li>
			<li class="centrado">
				<?php echo $classDay['ClassDay']['ejercicios']; ?>
			</li>
			<li class="centrado" data-role="list-divider">Otros (comentarios, ejercicios extra, etc)</li>
			<li class="centrado">

				<?php if( trim( $classDay['ClassDay']['comments'] ) != "" ): 
					echo $classDay['ClassDay']['comments'];
				else:
					echo "No hay comentarios extra";
				endif;?>
			</li>
			<li data-role="list-divider" class="centrado">
				Creado
			</li>
			<li class="centrado">
				<?php echo $this->Fechas->spanish($classDay['ClassDay']['created']); ?>
			</li>			
		</ul>
			<!-- Si hay usuario logueado muestro las opciones -->
			<a href="<?php echo $this->Html->url(array('controller'=>'class_days', 'action'=>'edit', $classDay['ClassDay']['id']));?>" 
				data-icon="plus" data-iconpos="left" data-inset="true" 
				data-role="button" data-theme="e">Editar Contenido</a>

	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->

	<div data-role="page" data-add-back-btn="true">
		<?php 
			echo $this->element('header', array('content'=>'Nuevo Día de Clase'));
		?>
		<div data-role="content">	
			<?php echo $this->Form->create('ClassDay'); ?>
		<fieldset>
			<center><?php echo __('Nuevo día de clase'); ?></center>
		<?php
			echo "<br/>";
			echo $this->Form->input('fecha', array('data-role'=>'datebox', 'data-options'=>'{"mode": "calbox"}'));
			echo "<br/>";
			echo $this->Form->input('teoria', array('label'=>'Páginas del libro de teoría'));
			echo "<br/>";
			echo $this->Form->input('ejercicios', array('label'=>'Páginas del libro de ejercicios'));
			echo "<br/>";
			echo $this->Form->input('comments', array('rows'=>10, 'label'=>'Otros (comentarios, ejercicios extra)'));
			//echo $this->Form->input('date');
		?>
		</fieldset>
		<?php echo $this->Form->end(__('Submit')); ?>
		</div><!-- /content -->
		<?php echo $this->element('footer'); ?>
	</div><!-- /page -->
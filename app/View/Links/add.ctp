<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Nuevo Usuario'));
	?>
	<div data-role="content">	
		<?php echo $this->Form->create('Link'); ?>
		<fieldset>
			<center><?php echo __('Nuevo Link'); ?></center>
		<?php
			echo $this->Form->input('title', array('label'=>'Título'));
			echo $this->Form->input('url', array('label'=>'Link'));
			echo $this->Form->input('comments', array('label'=>'Comentarios sobre el link', 'rows'=>10));
		?>
		</fieldset>
		<?php echo $this->Form->end(__('Submit')); ?>
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->

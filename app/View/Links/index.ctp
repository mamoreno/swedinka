<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Palabras'));
	?>
	<div data-role="content">	
		<ul data-role="listview" data-inset="true" data-filter="true">
			<li data-role="list-divider">Listado de Links</li>
			<?php foreach ($links as $i=>$link): 
				$l = $this->Html->url(array('controller'=>'links', 'action'=>'view', $link['Link']['id']));
			?>
				<li>
					<a href="<?php echo $l; ?>" >
						<h2> <?php echo $link['Link']['title']?></h2>
						<p><?php echo $link['Link']['comments']?></p>
					</a>
				</li>
			<?php endforeach;?>
		</ul>
		<!-- Si hay usuario logueado muestro las opciones -->
		<a href="<?php echo $this->Html->url(array('controller'=>'links', 'action'=>'add'));?>" 
			data-icon="plus" data-iconpos="left" data-inset="true" 
			data-role="button" data-theme="e">Nuevo Link</a>

	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->
<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Palabras'));
	?>
	<div data-role="content">
		<ul data-role="listview" data-inset="true" data-filter="false">
			<li data-role="list-divider" class="centrado">
				T&iacute;tulo
			</li>
			<li class="centrado">
				<?php echo $this->Util->uppFirst( $link['Link']['title'] ); ?>
			</li>
			<li class="centrado" data-role="list-divider">Link</li>
			<!-- <li class="centrado">
				<?php echo $link['Link']['url']; ?>
			</li>
 -->
			<li class="centrado">
				<?php echo $this->Html->link( "Abrir en nueva ventana", $link['Link']['url'], 
				array('target'=>'_blank') );?>
			</li>
			<li class="centrado" data-role="list-divider">Comentarios</li>
			<li class="centrado">

				<?php if( trim( $link['Link']['comments'] ) != "" ): 
					echo $link['Link']['comments'];
				else:
					echo "No hay comentarios sobre el link";
				endif;?>
			</li>
			<li data-role="list-divider" class="centrado">
				Creado
			</li>
			<li class="centrado">
				<?php echo $this->Fechas->spanish($link['Link']['created']); ?>
			</li>			
		</ul>

		<!-- Si hay usuario logueado muestro las opciones -->
		<a href="<?php echo $this->Html->url(array('controller'=>'links', 'action'=>'edit', $link['Link']['id']));?>" 
			data-icon="plus" data-iconpos="left" data-inset="true" 
			data-role="button" data-theme="e">Editar Link</a>

	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->



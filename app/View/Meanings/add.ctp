<div class="meanings form">
<?php echo $this->Form->create('Meaning'); ?>
	<fieldset>
		<legend><?php echo __('Add Meaning'); ?></legend>
	<?php
		echo $this->Form->input('word_id');
		echo $this->Form->input('meaning');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Meanings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Words'), array('controller' => 'words', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Word'), array('controller' => 'words', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="meanings view">
<h2><?php  echo __('Meaning'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($meaning['Meaning']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Word'); ?></dt>
		<dd>
			<?php echo $this->Html->link($meaning['Word']['word'], array('controller' => 'words', 'action' => 'view', $meaning['Word']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Meaning'); ?></dt>
		<dd>
			<?php echo h($meaning['Meaning']['meaning']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Meaning'), array('action' => 'edit', $meaning['Meaning']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Meaning'), array('action' => 'delete', $meaning['Meaning']['id']), null, __('Are you sure you want to delete # %s?', $meaning['Meaning']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Meanings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Meaning'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Words'), array('controller' => 'words', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Word'), array('controller' => 'words', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="meanings form">
<?php echo $this->Form->create('Meaning'); ?>
	<fieldset>
		<legend><?php echo __('Edit Meaning'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('word_id');
		echo $this->Form->input('meaning');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Meaning.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Meaning.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Meanings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Words'), array('controller' => 'words', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Word'), array('controller' => 'words', 'action' => 'add')); ?> </li>
	</ul>
</div>

<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$palabrasDescription = __('Palabras: by Mamoreno.com');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo"Palabras" ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php
		//echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');
		echo $this->Html->css('jquery-css/themes/default/jquery.mobile-1.2.0.min');
		echo $this->Html->css('jquery-css/themes/default/jquery.mobile.structure-1.2.0.min');
		echo $this->Html->css('jquery-css/themes/default/jquery.mobile.theme-1.2.0.min');
		echo $this->Html->css('jquery-css/fixes');
		// echo $this->Html->css('jquery-ui-css/ui-lightness/jquery-ui-1.9.2.custom');

		echo $this->Html->script('jquery');
		echo $this->Html->script('jquery-ui-1.9.2.custom');
		?>

		<script language="javascript">
      		$(document).bind("mobileinit", function () {
          		$.mobile.ajaxEnabled = false;
                $.mobile.page.prototype.options.degradeInputs.date = true;
      		});
		</script>
		
		<?php
			echo $this->Html->script('jquery.mobile-1.2.0');
			/*
			http://dev.jtsage.com/jQM-DateBox/
			*/
			echo $this->Html->css('datebox/jquery.mobile.datebox');
			echo $this->Html->script('jquery.mobile.datebox');
		?>
		<script type="text/javascript">
			// $(function () {
			//    jQuery.extend(jQuery.mobile.datebox.prototype.options, {
			//       'dateFormat': 'dd/MM/YYYY',
			//       'headerFormat': 'dd/MM/YYYY',
			//       'setDateButtonLabel': 'Aceptar',
			//       'fieldsOrder': ["d","m", "y"],
			//       'noButtonFocusMode': 'true'
			        
			//    }); 
			// });
		</script>

		<!-- 
			FIN DEL CALENDARIO
		-->
		<?php 
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>
</head>
<body>
	<?php echo $this->Session->flash(); ?>
	<?php echo $this->fetch('content'); ?>
	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>

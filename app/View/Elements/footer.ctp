<div id="footer" data-role="footer" data-position="fixed">
	<h1>
		<?php		
		if( $this->Session->check('Auth.User.id') ){
			echo $this->Html->link('Logout', 
				array('controller'=>'users', 'action'=>'logout'), 
				array('class'=>'ui-btn-left', 'data-role'=>'button')
				);
		}else{
			echo $this->Html->link('Login', 
				array('controller'=>'users', 'action'=>'login'), 
				array('class'=>'ui-btn-left', 'data-role'=>'button')
				);
		}
		?>
		<a href="http://www.mamoreno.com" data-role="button" data-icon="forward" 
		data-iconpos="right" target="_blank" data-theme="e" data-inline="true"
		class="ui-btn-right">
			by mamoreno.com
		</a>
	</h1>
</div><!-- /footer -->
<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Palabras'));
	?>
	<div data-role="content">

		<ul data-role="listview" data-inset="true" data-filter="false">

			<li data-role="list-divider">Palabra Suelta</li>
			<li class="centrado">
				<span class="grande">
				<?php echo $this->Html->image('sweden-flag.png', array('alt' => 'Sweden Flag', 'width'=>'20px'))?> 
				<?php echo $this->Util->uppFirst( $preposition['Preposition']['preposition'] ); ?>
				</span>
			</li>
			<li data-role="list-divider">Significado</li>
			<li class="centrado">
				<span>
				<?php echo $this->Html->image('spanish_flag.gif', array('alt' => 'Spanish Flag', 'width'=>'20px'))?>
				<?php echo $this->Util->uppFirst( $preposition['Preposition']['meaning'] ); ?>
				</span>
			</li>

			<li class="centrado" data-role="list-divider">Ejemplos</li>
			<li class="centrado">

				<?php if( trim( $preposition['Preposition']['examples'] ) != "" ): 
					echo $preposition['Preposition']['examples'];
				else:
					echo "No hay ejemplos";
				endif;?>
			</li>
			<li class="centrado" data-role="list-divider">Otros (comentarios, ejercicios extra, etc)</li>
			<li class="centrado">

				<?php if( trim( $preposition['Preposition']['comments'] ) != "" ): 
					echo $preposition['Preposition']['comments'];
				else:
					echo "No hay comentarios extra";
				endif;?>
			</li>
			<li data-role="list-divider" class="centrado">
				Creado
			</li>
			<li class="centrado">
				<?php echo $this->Fechas->spanish($preposition['Preposition']['created']); ?>
			</li>			
		</ul>
			<!-- Si hay usuario logueado muestro las opciones -->
			<a href="<?php echo $this->Html->url(array('controller'=>'preposition', 'action'=>'edit', $preposition['Preposition']['id']));?>" 
				data-icon="plus" data-iconpos="left" data-inset="true" 
				data-role="button" data-theme="e">Editar Contenido</a>

	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->
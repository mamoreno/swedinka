<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Palabras'));
	?>
	<div data-role="content">
		<ul data-role="listview" data-inset="true" data-filter="false">
			<li data-role="list-divider">Listado de Preposiciones</li>
			<?php foreach ($palabras as $inicial=>$palabros): ?>
				<li>
					<a href="#letra-<?php echo $inicial;?>">
					<?php echo $this->Util->uppFirst( $inicial ); ?>
					<span class="ui-li-count"> <?php echo $palabros['num'];?> </span>
					</a>
				</li>
			<?php endforeach;?>
		</ul>			
 		<!-- Si hay usuario logueado muestro las opciones -->
		<a href="<?php echo $this->Html->url(array('controller'=>'prepositions', 'action'=>'add'));?>" 
			data-icon="plus" data-iconpos="left" data-inset="true" 
			data-role="button" data-theme="e">Nueva Preposici&oacute;n</a>
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->




<?php foreach ($palabras as $inicial=>$palabros):?>
	<div data-role="page" id="letra-<?php echo $inicial;?>" data-add-back-btn="true">
		<?php echo $this->element('header', 
			array('content'=>
				$this->Html->link(
					'Palabras: '
					, array('controller' => 'words', 'action' => 'listar')
				) . "Letra " . strtoupper( $inicial ) 
			)
		); ?>
		<div data-role="content">	
			<ul data-role="listview" data-filter="true" data-inset="true">
				<li data-role="list-divider">Letra <?php echo $this->Util->uppFirst( $inicial );?></li>

				<?php array_shift( $palabros );
				foreach ($palabros as $palabra):?>
					<li>
						<?php echo $this->Html->link(
							$this->Util->uppFirst($palabra['Preposition']['preposition']),
					    array('controller' => 'prepositions', 'action' => 'view', $palabra['Preposition']['id']) );
						?>
					</li>
				<?php endforeach;?>
			</ul>
		<a href="<?php echo $this->Html->url(array('controller'=>'prepositions', 'action'=>'add'));?>" 
			data-icon="plus" data-iconpos="left" data-inset="true" 
			data-role="button" data-theme="e">Nueva Preposicion</a>

		</div>
		<?php echo $this->element('footer'); ?>
	</div>
<?php endforeach;?>
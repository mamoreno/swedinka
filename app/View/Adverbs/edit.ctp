<div class="adverbs form">
<?php echo $this->Form->create('Adverb'); ?>
	<fieldset>
		<legend><?php echo __('Edit Adverb'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('adverb');
		echo $this->Form->input('comments');
		echo $this->Form->input('meaning');
		echo $this->Form->input('examples');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Adverb.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Adverb.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Adverbs'), array('action' => 'index')); ?></li>
	</ul>
</div>

<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Login'));
	?>
	<div data-role="content">	
		<div class="users form">
			<?php echo $this->Session->flash('auth'); ?>
			<br/>
			<br/>
			<?php echo $this->Form->create('User'); ?>
		    <fieldset>
		        <div>
		        	<center><?php echo __('Please enter your username and password'); ?></center>
		        </div>
		        <br/>
				<br/>
		        <?php
		        	echo $this->Form->input('username', array('label'=>'Usuario'));
			        echo $this->Form->input('password');
			    ?>
		    </fieldset>
			<?php echo $this->Form->end(__('Login')); ?>
		</div>
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->
<!-- app/View/Users/add.ctp -->
<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Nuevo Usuario'));
	?>
	<div data-role="content">	
		<div class="users form">
		<?php echo $this->Form->create('User'); ?>
		    <fieldset>
		        <legend><?php echo __('Nuevo Usuario'); ?></legend>
		        <?php echo $this->Form->input('username');
		        echo $this->Form->input('password');
		        echo $this->Form->input('role', array(
		            //'options' => array('admin' => 'Admin', 'author' => 'Author')
		            'options' => array('author' => 'Author')
		        ));
				?>
		    </fieldset>
			<?php echo $this->Form->end(__('Enviar'), array('data-theme'=>'a')); ?>
		</div>
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->
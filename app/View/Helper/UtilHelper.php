<?php
App::uses('AppHelper', 'View/Helper');

class UtilHelper extends AppHelper {
  //  public $helpers = array();

    public function uppFirst($str) {
    	/*
			Hack para tomar la primera letra
			*/
			$pl = substr($str, 0, 2);
			$resto = substr($str, 2);
			if( $pl == 'ä' ) $pl = 'Ä';
			elseif( $pl == 'ö' ) $pl = 'Ö';
			elseif( $pl == 'å' ) $pl = 'Å';
			elseif( !in_array($pl, array('ä','ö','å', 'Ä', 'Ö', 'Å') ) ){
				$pl = strtoupper( substr($str, 0, 1) );
				$resto = substr($str, 1);

			}

        return $pl.$resto;
    }
}
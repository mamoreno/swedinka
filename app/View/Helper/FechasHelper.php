<?php
App::uses('AppHelper', 'View/Helper');

/* 
 * Fechas helper.   
 * Author: Fernando Dingler 
 */ 

class FechasHelper extends Helper{ 

	var $helpers = array(); 
     
	/****** For English just uncomment the lines indicated. ******/ 
         
    //Dada una fecha en formato yyyy-mm-dd 00:00:00 devuelve la fecha en un formato legible, ejemplo: 29 de 
    //Enero del 2009 
         
	function spanish($date){ 

		if($date == '' || empty($date)) 
 			return ''; 
     	$fa = explode(" ", $date);
     	$date = $fa[0];
		$meses = array("Ene" , "Feb" , "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", 
			"Nov", "Dic"); //Spanish 
			/* $meses = array("Jan" , "Feb" , "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct",  
				"Nov", "Dec");*/ //English 
         
		list($anio, $mes, $dia) = preg_split('/-/', $date); 
         
		$month = $meses[((int)$mes)-1]; 
         
		$fechaLegible = $dia." de ".$month." del ".$anio; //Spanish  
		/* $fechaLegible = $month.' '.$dia.", ".$anio; */ //English  
         
		return $fechaLegible; 
    } 
}

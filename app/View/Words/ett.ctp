<div data-role="page" data-add-back-btn="true" data-transition="slide">
	<?php echo $this->element('header', array('content'=>'Palabras')); ?>

	<div data-role="navbar">
		<ul> 
			<li><?php echo $this->Html->link('En-Palabras', array('controller'=>'words', 'action'=>'en')); ?></li>
			<li><a class="ui-btn-active" data-theme="b">Ett-Palabras</a></li>
		</ul>
	</div>	
	<div data-role="content">	
		<ul data-role="listview" data-filter="true">
			<?php foreach ($ett_palabras as $palabra):?>
				<li>
					<?php echo $this->Html->link(
				    $this->Util->uppFirst( $palabra['Word']['word'] ),
				    array('controller' => 'words', 'action' => 'showWord', $palabra['Word']['id']) );
					?>
				</li>
			<?php endforeach;?>
		</ul>
	</div>
	<?php echo $this->element('footer'); ?>
		
</div>

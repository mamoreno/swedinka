<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Tag: ' . $tag . " ( " . $numPalabras ." ) "));
	?>
	<div data-role="content">	
		<ul data-role="listview" data-inset="true" data-filter="true">
			<?php foreach ($palabras as $palabra):?>
				<li> <?php echo $this->Html->link($this->Util->uppFirst($palabra['Word']['word']), 
				array('controller'=>'words', 'action'=>'showWord', $palabra['Word']['id']) ); ?>
			<?php endforeach;?>
		</ul>			
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->

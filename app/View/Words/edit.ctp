
<div data-role="page" data-add-back-btn="true" data-transition="slide" data-ajax="false">
	<?php echo $this->element('header', array('content'=>'Palabras: Nueva')); ?>
	<div data-role="content">	
		<?php echo $this->Form->create('Word'); ?>
	<fieldset>
		<legend><?php echo __('Add Word'); ?></legend>
	<?php
		function nl(){ echo "<br/>"; }
		nl();
		echo $this->Form->hidden('Id');

		echo $this->Form->input('genre', array('data-role'=>'slider', 'type'=>'select', 'label'=>'Género',
			'options'=>array('en'=>'En', 'ett'=>'Ett')));
		nl();
		echo $this->Form->input('word', array('label'=>'Palabra en sueco'));

		nl();
		echo $this->Form->input('Meaning',
			 array('label'=>'Significado', 'type'=>'text','value'=>$meanings['Meaning']['meaning']));

		nl();
		echo $this->Form->input('plural');

		nl();
		echo $this->Form->input('articled', array('label'=>'Palabra articulada'));

		nl();
		echo $this->Form->input('articled_plural', array('label'=>'Articulada y en plural'));

		nl();
		echo $this->Form->input('comments', array('label'=>'¿Comentarios?'));

		nl();
		echo $this->Form->input('Tag', array('label'=>'Tags (separadas por comas)'
			, 'type'=>'text', 'value'=>$tags));

		nl();
		echo $this->Form->input('Image', 
			array('label'=>'Imagen( Url de la imagen )', 'type'=>'text', 'value'=>$images['Image']['url']));
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
	</div>
	<?php echo $this->element('footer'); ?>		
</div>






<div class="words form">
<?php echo $this->Form->create('Word'); ?>
	<fieldset>
		<legend><?php echo __('Edit Word'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('genre');
		echo $this->Form->input('word');
		echo $this->Form->input('plural');
		echo $this->Form->input('articled');
		echo $this->Form->input('articled_plural');
		echo $this->Form->input('comments');
		echo $this->Form->input('Tag');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Word.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Word.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Words'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Images'), array('controller' => 'images', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Image'), array('controller' => 'images', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Meanings'), array('controller' => 'meanings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Meaning'), array('controller' => 'meanings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tags'), array('controller' => 'tags', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tag'), array('controller' => 'tags', 'action' => 'add')); ?> </li>
	</ul>
</div>

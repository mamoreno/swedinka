<div data-role="page" data-add-back-btn="true">
	<?php echo $this->element('header', array('content'=>$palabra['Word']['word'])); ?>

	<div data-role="content">
	<ul data-role="listview" data-inset="true" data-filter="false">
		<li data-role="list-divider">Info de palabra</li>
		<li class="centrado">
			<span class="grande">
			<?php echo $this->Html->image('sweden-flag.png', array('alt' => 'Sweden Flag', 'width'=>'20px'))?> 
			<?php echo $this->Util->uppFirst( $palabra['Word']['word'] ); ?>
			</span>
		</li>
		<li data-role="list-divider">Significado</li>
		<li class="centrado">
			<span>
			<?php echo $this->Html->image('spanish_flag.gif', array('alt' => 'Spanish Flag', 'width'=>'20px'))?>
			<?php echo $this->Util->uppFirst( $meanings ); ?>
			</span>
		</li>
		<li>
			<?php
				$url = $this->Html->url(array('controller'=>'words', 'action'=>$palabra['Word']['genre']));
				$texto = $this->Util->uppFirst( $palabra['Word']['genre'] ) . " (Pulsa para filtrar por genero)";
				$texto = $this->Util->uppFirst( $palabra['Word']['genre'] );
			 ?>
			<a href="<?php echo $url ?>">
				G&eacute;nero<span class="ui-li-aside"><?php echo $texto; ?></span>
			</a>
		</li>
		<li>
			Articulada<span class="ui-li-aside"><?php echo $this->Util->uppFirst( $palabra['Word']['articled'] ); ?></span>
		</li>
		<li>
			Plural<span class="ui-li-aside"><?php echo $this->Util->uppFirst( $palabra['Word']['plural'] ); ?></span>
		</li>

		<li>
			Plural Articulado<span class="ui-li-aside"><?php echo $this->Util->uppFirst( $palabra['Word']['articled_plural'] ); ?></span>
		</li>
		<li data-role="list-divider">Comentarios</li>
		<?php if( trim( $palabra['Word']['comments'] ) != "" ):?>
			<li class="centrado"><?php echo $this->Util->uppFirst( $palabra['Word']['comments'] ); ?></li>
		<?php else:?>
			<li class="centrado">No hay comentarios</li>
		<?php endif; ?>
		<li data-role="list-divider">Im&aacute;genes</li>
		<?php if( isset($palabra['Image'][0]) ):?>
			<?php if(count( $palabra['Image']) > 1 ):?>
				<li class="centrado">Ver listado de im&aacute;genes
					<ul>
						<?php foreach ($palabra['Image'] as $image): ?>
						<li class="centrado">
						<a href="<?php echo $image['url']; ?>" target="_blank">Ver imagen en ventana nueva</a></li>
						<?php endforeach; ?>
					</ul>
				</li>
				<?php else:?>
					<li class="centrado">
						<?php $image_url=$palabra['Image'][0]['url']; ?>
						<a href="<?php echo $image_url;?>" target="_blank">Ver imagen en ventana nueva</a>
					</li>
				<?php endif;?>
			
		<?php else: ?>
			<li class="centrado">No hay im&aacute;genes.</li>
		<?php endif; ?>
		<li data-role="list-divider">Tags</li>
		<?php if( count($palabra['Tag']) == 1): ?>
			<li class="centrado">
			 <?php echo $this->Html->link( $palabra['Tag'][0]['tag'], 
			 		array('controller'=>'words', 'action'=>'listarPorTag', $palabra['Tag'][0]['id']) );?>
			</li>
		<?php else: ?>
		<li class="centrado">
			<a href="#tags-page"><?php echo $tags; ?></a>
		</li>
		<?php endif; ?>
		<li data-role="list-divider" class="centrado">
				Creado
			</li>
			<li class="centrado">
				<?php echo $this->Fechas->spanish($palabra['Word']['created']); ?>
			</li>			

	</ul>
		<a href="<?php echo $this->Html->url(array('controller'=>'words', 'action'=>'edit', $palabra['Word']['id']));?>" 
			data-icon="plus" data-iconpos="left" data-inset="true" 
			data-role="button" data-theme="e">Editar Palabra</a>
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>

</div><!-- /page -->



<div data-role="page" data-add-back-btn="true" id="tags-page">
	<?php 
		echo $this->element('header', array('content'=>'Palabras'));
	?>
	<div data-role="content">
		<ul data-role="listview" data-inset="true" data-filter="true">
				<?php foreach($palabra['Tag'] as $tag): ?>
				<li>
				 <?php echo $this->Html->link( $tag['tag'], 
				 		array('controller'=>'words', 'action'=>'listarPorTag', $tag['id']) );?>
				</li>
				<?php endforeach; ?>
			</ul>
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->
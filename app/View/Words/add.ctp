<div data-role="page" data-add-back-btn="true" data-transition="slide" data-ajax="false">
	<?php echo $this->element('header', array('content'=>'Palabras: Nueva')); ?>
	<div data-role="content">	
		<?php echo $this->Form->create('Word'); ?>
	<fieldset>
		<legend><?php echo __('Add Word'); ?></legend>
	<?php
		function nl(){ echo "<br/>"; }
		nl();
		echo $this->Form->input('genre', array('data-role'=>'slider', 'type'=>'select', 'label'=>'Género',
			'options'=>array('en'=>'En', 'ett'=>'Ett')));
		nl();
		echo $this->Form->input('word', array('label'=>'Palabra en sueco'));

		nl();
		echo $this->Form->input('meaning', array('label'=>'Significado'));

		nl();
		echo $this->Form->input('plural');

		nl();
		echo $this->Form->input('articled', array('label'=>'Palabra articulada'));

		nl();
		echo $this->Form->input('articled_plural', array('label'=>'Articulada y en plural'));

		nl();
		echo $this->Form->input('comments', array('label'=>'¿Comentarios?'));

		nl();
		echo $this->Form->input('myTags', array('label'=>'Tags (separadas por comas)', 
			'type'=>'text', 'value'=>'NB2,'));

		nl();
		echo $this->Form->input('image', array('label'=>'Imagen( Url de la imagen )', 'type'=>'text'));
	?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
	</div>
	<?php echo $this->element('footer'); ?>		
</div>
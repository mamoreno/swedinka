<div data-role="page" data-add-back-btn="true" data-transition="slide">
	<?php echo $this->element('header', array('content'=>'Palabras')); ?>
	<div data-role="content">

		<div id="pregunta">
			
		</div>
		<ul data-role="listview">
			<li data-role="list-divider">
				<div class="grandote centrado">En eller Ett????</div>
			</li>
			<li data-theme="e">
				<div class="grandote centrado">
					<?php echo $this->Util->uppFirst( $palabra['word'] );?>
				</div>
			</li>
			<li id="respuesta" class = "grandote">
			</li>
			<li class="btn">
				<center>
					<div data-role="controlgroup" data-type="horizontal">
						<a href="javascript:check('en')" data-role="button" data-theme="b" >
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							En
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</a>
						<a href="javascript:check('ett')" data-role="button" data-theme="b">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							Ett
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</a>
					</div>
					<br/>
					<br/>

					<a href="<?php echo $this->Html->url(array('controller'=>'words', 'action'=>'enEllerEtt')) ;?>" 
					data-role="button" data-inset="false" data-theme="b">Ver otra palabra</a>

				</center>
			</li>
		</ul>
		<br/>
		<br/>
		<div data-role="controlgroup" data-type="vertical" class="btn_extra">
			<a href="<?php echo $this->Html->url(array('controller'=>'words', 'action'=>'enEllerEtt')) ;?>" 
					data-role="button" data-inset="false" data-theme="b">Otra palabra</a>
			<a href="<?php echo $this->Html->url(
				array('controller'=>'words', 'action'=>'showWord', $palabra['id'])) ;?>" 
					data-role="button" data-inset="false" data-theme="b">Ver palabra</a>
		</div>


	</div>
	<?php echo $this->element('footer'); ?>
</div>

<script type="text/javascript">
	function check(selected){
		var respuesta = "<?php echo $palabra['genre'];?>";

		var respElem = $('#respuesta');
		var bien = "<div class='respuesta bien'>¡¡BIEN!!</div>";
		var mal = "<div class='respuesta mal'>MAL...</div>";
		if( respuesta == selected ){
			respElem.html(bien);
		}else{
			respElem.html(mal);
		}
		$('.btn').toggle();
		$('.btn_extra').toggle();
	}
</script>

<style type="text/css">
	.grandote{
		height: 50px;
		margin-top: 25px;
		text-align: center;
		font-weight: bold;
		border-radius: 5px;
		font-size: 150%;
	}
	.respuesta{
		font-weight: bold;
		font-size: 150%;
		margin: auto;
		text-align: center;
	}
	.bien{
		color: green;
	}

	.mal{
		color: red;
	}
	.btn_extra{
		display: none;
	}
</style>
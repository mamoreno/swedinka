<div data-role="page" data-add-back-btn="true" data-transition="slide">
	<?php echo $this->element('header', array('content'=>'Palabras')); ?>
	<div data-role="content">

		<div id="pregunta">
			
		</div>
		<ul data-role="listview">
			<li data-role="list-divider">
				<div class="grandote centrado">¿Cuál es el plural?</div>
			</li>
			<li data-theme="e">
				<div class="grandote centrado">
					<?php echo $this->Util->uppFirst( $palabra['word'] );?>
				</div>
			</li>
			<li id="respuesta" class = "grandote">
			</li>
		</ul>
		<br/>
		<br/>
		<center>

			<div class="btn">
				<label>Introduce el plural</label>
				<input id="user_res" type="text" placeholder="plural?" size="10">
			</div>

			<div data-role="controlgroup" data-type="horizontal" class="btn">
				<a href="<?php echo $this->Html->url(array('controller'=>'words', 'action'=>'guessPlural')) ;?>" 
						data-role="button" data-inset="false" data-theme="b">Ver otra palabra</a>
				<a href="javascript:check()" data-role="button" data-inset="false" data-theme="b">Ya esta</a>

			</div>
			<div data-role="controlgroup" data-type="vertical" class="btn_extra">
				<a href="<?php echo $this->Html->url(array('controller'=>'words', 'action'=>'guessPlural')) ;?>" 
						data-role="button" data-inset="false" data-theme="b">Otra palabra</a>
				<a href="<?php echo $this->Html->url(
					array('controller'=>'words', 'action'=>'showWord', $palabra['id'])) ;?>" 
						data-role="button" data-inset="false" data-theme="b">Ver palabra</a>
			</div>
		</center>

	</div>
	<?php echo $this->element('footer'); ?>
</div>

<script type="text/javascript">
	function check(){
		var user_response = $('#user_res').val();
		var respuesta = "<?php echo $palabra['plural'];?>";
		respuesta = respuesta.toLowerCase();
		user_response = user_response.toLowerCase();

		var respElem = $('#respuesta');
		var bien = "<div class='respuesta bien'>¡¡BIEN!!</div>";
		var mal = "<div class='respuesta mal'>MAL... era: " + respuesta + "</div>";
		if( respuesta == user_response ){
			respElem.html(bien);
		}else{
			respElem.html(mal);
		}
		$('.btn').toggle();
		$('.btn_extra').toggle();
	}
</script>

<style type="text/css">
	.grandote{
		height: 35px;
		margin-top: 25px;
		text-align: center;
		font-weight: bold;
		border-radius: 5px;
		font-size: 120%;
	}
	.respuesta{
		font-weight: bold;
		font-size: 150%;
		margin: auto;
		text-align: center;
	}
	.bien{
		color: green;
	}

	.mal{
		color: red;
	}
	.btn_extra{
		display: none;
	}
</style>
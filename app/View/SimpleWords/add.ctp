<div data-role="page" data-add-back-btn="true">
	<?php 
		echo $this->element('header', array('content'=>'Nuevo Palabra Suelta'));
	?>
	<div data-role="content">	
		<div class="users form">
		<?php echo $this->Form->create('SimpleWord'); ?>
		<fieldset>
			<legend><?php echo __('Nueva Palabra Suelta'); ?></legend>
		<?php
			echo $this->Form->input('word');
			echo $this->Form->input('comments');
			echo $this->Form->input('meaning');
			echo $this->Form->input('examples');
		?>
		</fieldset>
			<?php echo $this->Form->end(__('Enviar'), array('data-theme'=>'a')); ?>
		</div>
	</div><!-- /content -->
	<?php echo $this->element('footer'); ?>
</div><!-- /page -->
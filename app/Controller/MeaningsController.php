<?php
App::uses('AppController', 'Controller');
/**
 * Meanings Controller
 *
 * @property Meaning $Meaning
 */
class MeaningsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Meaning->recursive = 0;
		$this->set('meanings', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Meaning->id = $id;
		if (!$this->Meaning->exists()) {
			throw new NotFoundException(__('Invalid meaning'));
		}
		$this->set('meaning', $this->Meaning->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Meaning->create();
			if ($this->Meaning->save($this->request->data)) {
				$this->Session->setFlash(__('The meaning has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The meaning could not be saved. Please, try again.'));
			}
		}
		$words = $this->Meaning->Word->find('list');
		$this->set(compact('words'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Meaning->id = $id;
		if (!$this->Meaning->exists()) {
			throw new NotFoundException(__('Invalid meaning'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Meaning->save($this->request->data)) {
				$this->Session->setFlash(__('The meaning has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The meaning could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Meaning->read(null, $id);
		}
		$words = $this->Meaning->Word->find('list');
		$this->set(compact('words'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Meaning->id = $id;
		if (!$this->Meaning->exists()) {
			throw new NotFoundException(__('Invalid meaning'));
		}
		if ($this->Meaning->delete()) {
			$this->Session->setFlash(__('Meaning deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Meaning was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

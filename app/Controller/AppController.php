<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $mamoreno = "http://www.mamoreno.com/palabras/";
    public $debug=true;
    public $helpers = array('Util', 'Fechas');
    public $components = array(
        'Session'
        ,'Auth' => array(
            'loginRedirect' => array('controller' => 'pages', 'action' => 'display'),
            'logoutRedirect' => array('controller' => 'pages', 'action' => 'display'),
            'authorize' => array('Controller') // Added this line
        )
        ,'Email'=>array(
           'smtpOptions' => array(
                // 'port'=>'465',
                // 'timeout'=>'30',
                'host'=>'mail.mamorneo.com',
                'username'=>'mamor',
                'password'=>'miHai_77'
            )
        )
    );

    public function beforeFilter() {
        $this->Auth->allow('display', 'index', 'view', 'logout'); //para la home y los listados
    }

    public function isAuthorized($user) {
        // Admin can access every action
        if (isset($user['role']) && $user['role'] === 'admin') {
            return true;
        }

        // Default deny
        return false;
    }


    public function mandaMail($subject, $contenido, $link, $debug = false){

        $this->Email->smtpOptions = array(
                    'port'=>'465',
                    // 'timeout'=>'30',
                    'host'=>'ssl://smtp.gmail.com',
                    'username'=>'miguel.s.address@gmail.com',
                    'password'=>'miHai_77'
                    );

        $this->Email->delivery = 'smtp';
        if( $debug){
            $this->Email->to      = 'Yo <miguel.s.address@gmail.com>';
            $subject = "DEBUG - " . $subject;
        }else{
            $this->Email->to      = 'Sueco NB2 <svenska-nb2@googlegroups.com>';            
        }

        $this->Email->cc      = 'Yo <miguel.s.address@gmail.com>';

           // $this->Email->to      = 'Swedinka <miguel.s.address@gmail.com>';
        // $this->Email->from      = 'Swedinka <miguel.s.address@gmail.com>';
        $this->Email->from      = 'Swedinka <mamor@mamoreno.com>';
        $this->Email->subject = $subject;
        $contenido .= "\n\nPuedes ver el contenido siguiendo este enlace: $link";
        $this->Email->send($contenido);
    }
}

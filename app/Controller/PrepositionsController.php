<?php
App::uses('AppController', 'Controller');
/**
 * Prepositions Controller
 *
 * @property Preposition $Preposition
 */
class PrepositionsController extends AppController {

/**
  * lista las palabras en orden alfabetico, ordenadas por letra
  *
  * @param char $letra: Letra a filtrar
  * @return void
  *
  */

    public function index( $letra = null  ){

        $words = $this->Preposition->find('all');

        $palabras = array();
        $numPalabras = 0;
        $n=1; //num de palabras según letra
        $l="a";
        foreach ($words as $word) {
            $w = $word[$this->modelClass]['preposition'];
            //primera letra de la palabra

            /*
            Hack para tomar la primera letra
            */
            $pl = substr($w, 0, 2);
            if( !in_array($pl, array('ä','ö','å', 'Ä', 'Ö', 'Å') ) ){
                $pl = substr($w, 0, 1);
            }
            /**
             * ¿Por que este hack? Esta identificando las letrs especiales como
             * dos caracteres, por lo que si solo me quedo con uno, no identifica nada
             * y muestra un ? en lugar de a con dos puntos
             * Primero pruebo si es una especial y si no lo es, pues retransformo, porque
             * si no, tendria dos letras en lugar de una, es decir, "an" en lugar de "a"
             * por haber cogido dos en el substr
             */
            if( $letra ){
                 $this->set("cabecera", "Listado para ". $letra);

            }
            if( ( $letra ) && ( $pl != $letra ) ) continue;

            if( !isset( $palabras[$pl] ) ){
                    $palabras[$pl] = array();
                    $palabras[$pl]['num']=0;
            }
            $palabras[$pl]['num']++;
            $palabras[$pl][] = $word;
            $numPalabras++;
        }
        ksort($palabras);
        $this->set("palabras", $palabras);
        $this->set("numPalabras", $numPalabras);
    }


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Preposition->id = $id;
		if (!$this->Preposition->exists()) {
			throw new NotFoundException(__('Invalid preposition'));
		}
		$this->set('preposition', $this->Preposition->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Preposition->create();
			if ($this->Preposition->save($this->request->data)) {
				$this->Session->setFlash(__('The preposition has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The preposition could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Preposition->id = $id;
		if (!$this->Preposition->exists()) {
			throw new NotFoundException(__('Invalid preposition'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Preposition->save($this->request->data)) {
				$this->Session->setFlash(__('The preposition has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The preposition could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Preposition->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Preposition->id = $id;
		if (!$this->Preposition->exists()) {
			throw new NotFoundException(__('Invalid preposition'));
		}
		if ($this->Preposition->delete()) {
			$this->Session->setFlash(__('Preposition deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Preposition was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function isAuthorized($user) {
	    // All registered users can add posts
	    if (in_array( $this->action, array('add', 'edit') )) {
	        return true;
	    }

	    return parent::isAuthorized($user);
	}
}

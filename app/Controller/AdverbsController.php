<?php
App::uses('AppController', 'Controller');
/**
 * Adverbs Controller
 *
 * @property Adverb $Adverb
 */
class AdverbsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Adverb->recursive = 0;
		$this->set('adverbs', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Adverb->id = $id;
		if (!$this->Adverb->exists()) {
			throw new NotFoundException(__('Invalid adverb'));
		}
		$this->set('adverb', $this->Adverb->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Adverb->create();
			if ($this->Adverb->save($this->request->data)) {
				$this->Session->setFlash(__('The adverb has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The adverb could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Adverb->id = $id;
		if (!$this->Adverb->exists()) {
			throw new NotFoundException(__('Invalid adverb'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Adverb->save($this->request->data)) {
				$this->Session->setFlash(__('The adverb has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The adverb could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Adverb->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Adverb->id = $id;
		if (!$this->Adverb->exists()) {
			throw new NotFoundException(__('Invalid adverb'));
		}
		if ($this->Adverb->delete()) {
			$this->Session->setFlash(__('Adverb deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Adverb was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}

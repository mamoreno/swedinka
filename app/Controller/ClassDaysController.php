<?php
App::uses('AppController', 'Controller');
/**
 * ClassDays Controller
 *
 * @property ClassDay $ClassDay
 */
class ClassDaysController extends AppController {

var $debug = false;
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ClassDay->recursive = 0;
		$this->set('classDays', $this->ClassDay->find('all', array('order'=>'date ASC')));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ClassDay->id = $id;
		if (!$this->ClassDay->exists()) {
			throw new NotFoundException(__('Invalid class day'));
		}
		$this->set('classDay', $this->ClassDay->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ClassDay->create();
			/* poner la fecha en el formato correcto */
			$f = $this->request->data['ClassDay']['fecha'];
			$af = explode("-", $f);
			$d = $af[2]."-".$af[1]."-".$af[0];
			$this->request->data['ClassDay']['date']=$d;
			/* fin */
			if ($this->ClassDay->save($this->request->data)) {
				$link = $this->mamoreno. "class_days/view/" . $this->ClassDay->id;
				$this->mandaMail("Swedinka - Nueva info", "Se ha añadido información de lo que se " 
					. "ha hecho en clase el día $f", $link, $this->debug);
				$this->Session->setFlash(__('The class day has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The class day could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ClassDay->id = $id;
		if (!$this->ClassDay->exists()) {
			throw new NotFoundException(__('Invalid class day'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			/* poner la fecha en el formato correcto */
			$f = $this->request->data['ClassDay']['fecha'];
			$af = explode("-", $f);
			$d = $af[2]."-".$af[1]."-".$af[0];
			$this->request->data['ClassDay']['date']=$d;
			/* fin */

			if ($this->ClassDay->save($this->request->data)) {
				$link = $this->mamoreno. "class_days/view/" . $this->ClassDay->id;
				$this->mandaMail("Swedinka - Editada info", "Se ha editado la información de lo que se " 
					. "ha hecho en clase el día $f.", $link, $this->debug);

				$this->Session->setFlash(__('The class day has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The class day could not be saved. Please, try again.'));
			}
		} else {
			$cd = $this->ClassDay->read(null, $id);
			/* poner la fecha en el formato correcto */
			$f = $cd['ClassDay']['date'];
			$af = explode("-", $f);
			$d = $af[2]."-".$af[1]."-".$af[0];
			$cd['ClassDay']['date']=$d;
			/* fin */
			$this->set('fecha', $d);
			$this->request->data = $cd;
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ClassDay->id = $id;
		if (!$this->ClassDay->exists()) {
			throw new NotFoundException(__('Invalid class day'));
		}
		if ($this->ClassDay->delete()) {
			$this->Session->setFlash(__('Class day deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Class day was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


	public function isAuthorized($user) {
	    // All registered users can add posts
	    if (in_array( $this->action, array('add', 'edit') )) {
	        return true;
	    }

	    return parent::isAuthorized($user);
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * Words Controller
 *
 * @property Word $Word
 */
class WordsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->redirect(array('action'=>'listar'));

		$this->Word->recursive = 0;
		$this->set('words', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->redirect(array('action'=>'showWord', $id));

		$this->Word->id = $id;
		if (!$this->Word->exists()) {
			throw new NotFoundException(__('Invalid word'));
		}
		$this->set('word', $this->Word->read(null, $id));
	}


	function nueva(){
		$this->redirect(array('action'=>'add'));
	}
/**
 * add method
 *
 * @return void
 */
	public function add() {

		if ($this->request->is('post')) {
			$datos = $this->request->data['Word'];
			$img_url = trim( $datos['image'] );
			$tags_aux = explode(",", trim( $datos['myTags']) );
			$tags = array();
			foreach ($tags_aux as $t ) {
				if( strtolower( trim($t) ) )
					$tags[]= strtolower( trim($t) );
			}
			$meaning = trim( $datos['meaning'] );
			$palabra = $this->Word->findByWord( trim( $datos['word'] ) );
			if($palabra){
				$this->Session->setFlash(__('The word already exists.'));
				$this->redirect(array('controller'=>'pages', 'action' => 'display', 'existe_palabra'));
			}
			$palabra = array(
				'Word'=>array(
					'genre'=>$datos['genre']
					,'word'=>trim( $datos['word'])
					,'plural'=>trim($datos['plural'])
					,'articled'=>trim($datos['articled'])
					,'articled_plural'=>trim($datos['articled_plural'])
					,'comments'=>trim($datos['comments'])
					,'user_id'=>$this->Auth->user('id')
					)
			);
			
			$this->Word->create();

			if( !$this->Word->save( $palabra ) ){
				$this->Session->setFlash(__('The word could not be saved. Please, try again.'));
			}
			$word_id = $this->Word->id;
			
			// guardo el significado
			$significado = array(
				'Meaning' => array(
					'meaning'=> $meaning
					,'word_id'=>$word_id
					)
			);
			$this->Word->Meaning->save( $significado );


			// guardo la imagen
			$imagen = array(
				'Image'=>array(
					'url' => $img_url
					,'word_id'=>$word_id
					)
			);
			$this->Word->Image->save( $imagen );
			
			$tags_ids = array();
			$this->Word->Tag->recursive=0;
			foreach ($tags as $tag) {
				$t = $this->Word->Tag->findByTag( $tag );
				if( $t ){
					$tags_ids[ $tag ]=$t['Tag']['id'];
				}else{
					$unTag = array( 'Tag' => array( 'tag'=>$tag ) );
					$this->Word->Tag->save($unTag);
					$tags_ids[ $tag ]= $this->Word->Tag->id;
				}
			}

			//ahora relaciono la palabra con sus tags!!

			/*
			* Falta ver si ya existe la asociacion!!
			*/
			foreach ($tags_ids as $tag_id) {
				$newTagWord = array(
					'tag_id' => $tag_id
					,'word_id' => $word_id
				);
				$this->Word->TagsWord->create();
				$this->Word->TagsWord->save( $newTagWord );
			}

			$this->log('El usuario: ' . $this->Auth->user('username') . " - ha creado la palabra: " . $datos['word'] , 'app_info');
			$this->redirect( array('action'=>'showWord', $word_id) );

			$this->Session->setFlash(__('The word has been saved'));
	}
}

function guardar( $datos, $word_id=0 ){

	$img_url = trim( $datos['Word']['Image'] );
	$tags_aux = explode(",", trim( $datos['Tag']['Tag']) );
	$tags = array();
	foreach ($tags_aux as $t ) {
		if( strtolower( trim($t) ) )
			$tags[]= strtolower( trim($t) );
	}
	$meaning = trim( $datos['Word']['Meaning'] );

	$palabra = array(
		'Word'=>array(
			'genre'=>$datos['Word']['genre']
			,'word'=>trim( $datos['Word']['word'])
			,'plural'=>trim($datos['Word']['plural'])
			,'articled'=>trim($datos['Word']['articled'])
			,'articled_plural'=>trim($datos['Word']['articled_plural'])
			,'comments'=>trim($datos['Word']['comments'])
			,'user_id'=>$this->Auth->user('id')
			)
		);
	if($word_id){
		$palabra['Word']['word_id'] = $word_id;
	}
			
	//$this->Word->create();

	if( !$this->Word->save( $palabra ) ){
		$this->Session->setFlash(__('The word could not be saved. Please, try again.'));
	}
			
	// guardo el significado
	$sig = $this->Word->Meaning->find( 'all', array('conditions'=>array('word_id'=>$word_id) ) );
	if($sig){
		foreach ($sig as $s) {
			$this->Word->Meaning->delete( $s['Meaning']['id'] );
		}
	}
	$significado = array(
		'Meaning' => array(
			'meaning'=> $meaning
			,'word_id'=>$word_id
			)
	);

	$this->Word->Meaning->save( $significado );


	// guardo la imagen
	$im = $this->Word->Image->find( 'all', array('conditions'=>array('word_id'=>$word_id) ) );

	if($im){
		foreach ($im as $i) {
			$this->Word->Image->delete( $i['Image']['id'] );
		}
	}
	$imagen = array(
		'Image'=>array(
		'url' => $img_url
		,'word_id'=>$word_id
		)
	);

	$this->Word->Image->save( $imagen );
			
	$tags_ids = array();
	$this->Word->Tag->recursive=0;
	foreach ($tags as $tag) {
		$t = $this->Word->Tag->findByTag( $tag );
		if( $t ){
			$tags_ids[ $tag ]=$t['Tag']['id'];
		}else{
			$unTag = array( 'Tag' => array( 'tag'=>$tag ) );
			$this->Word->Tag->save($unTag);
			$tags_ids[ $tag ]= $this->Word->Tag->id;
		}
	}

	//ahora relaciono la palabra con sus tags!!
	
	$this->Word->Tag->recursive=0;
	$ntw = $this->Word->TagsWord->find('all', array('conditions' => array('word_id'=>$word_id)) );
	foreach ($ntw as $tw) {
		$this->Word->TagsWord->delete($tw['TagsWord']['id']);
	}
	foreach ($tags_ids as $tag_id) {

		$newTagWord = array(
			'tag_id' => $tag_id
			,'word_id' => $word_id
		);
		$this->Word->TagsWord->create();
		$this->Word->TagsWord->save( $newTagWord );
	}
	$this->Session->setFlash(__('The word has been saved'));
	$this->redirect( array('action'=>'showWord', $word_id) );
}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Word->id = $id;
		if (!$this->Word->exists()) {
			throw new NotFoundException(__('Invalid word'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Word']['Id'] = $id;
			$this->guardar( $this->request->data, $id );
			$this->log('El usuario: ' . $this->Auth->user('username') . " - ha editado la palabra: " 
				. $this->request->data['Word']['word']  . " con id: " . $id, 'app_info');
			$this->redirect( array('action'=>'showWord', $id) );

		} else {
			$this->request->data = $this->Word->read(null, $id);
		}
		$tags_aux = $this->Word->TagsWord->find('all', array('conditions'=>array('TagsWord.word_id'=>$id)));
		if( $tags_aux ){
			$tag_list = array();
			foreach ($tags_aux as $t) {
				$tt = $this->Word->Tag->findById($t['TagsWord']['tag_id']);
				$tag_list[]=$tt['Tag']['tag'];
			}
			$tags=implode(", ",$tag_list);

		}else{
			$tags="";
		} 
		$meanings = $this->Word->Meaning->findByWordId( $id, array('fields'=>'meaning'));
		$images = $this->Word->Image->findByWordId( $id, array('fields'=>'url'));
		$this->set(compact('tags'));
		$this->set(compact('meanings'));
		$this->set(compact('images'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Word->id = $id;
		if (!$this->Word->exists()) {
			throw new NotFoundException(__('Invalid word'));
		}
		if ($this->Word->delete()) {
			$this->Session->setFlash(__('Word deleted'));
			$this->redirect(array('action' => 'listar'));
		}
		$this->Session->setFlash(__('Word was not deleted'));
		$this->redirect(array('action' => 'listar'));
	}


/**
 * lista las palabras en orden alfabetico, ordenadas por letra
 *
 * @param char $letra: Letra a filtrar
 * @return void
 *
 */

	public function listar( $letra = null, $tag_id=null ){
		//if( $tag_id==0 ) throw new NotFoundException(__('Invalid tag id'));

		if( $tag_id ){
			$this->Word->Tag->recursive = 1;
			$tag = $this->Word->Tag->findById( $tag_id );
			if( !$tag ) throw new NotFoundException(__('Invalid tag id'));
			foreach ($tag['Word'] as $w) {
				$words[] = array('Word' => $w);
			}

			$this->set("cabecera", "Listado para " . $tag['Tag']['tag']);
		}else{
			$this->Word->recursive = 0;
			$words = $this->Word->find('all');

		}

		$palabras = array();
		$numPalabras = 0;
		$n=1; //num de palabras según letra
		$l="a";
		foreach ($words as $word) {
			$w = $word['Word']['word'];
			//primera letra de la palabra

			/*
			Hack para tomar la primera letra
			*/
			$pl = substr($w, 0, 2);
			if( !in_array($pl, array('ä','ö','å', 'Ä', 'Ö', 'Å') ) ){
				$pl = substr($w, 0, 1);
			}
			/**
			 * ¿Por que este hack? Esta identificando las letrs especiales como
			 * dos caracteres, por lo que si solo me quedo con uno, no identifica nada
			 * y muestra un ? en lugar de a con dos puntos
			 * Primero pruebo si es una especial y si no lo es, pues retransformo, porque
			 * si no, tendria dos letras en lugar de una, es decir, "an" en lugar de "a"
			 * por haber cogido dos en el substr
			 */
			if( $letra ){
				 $this->set("cabecera", "Listado para ". $letra);

			}
			if( ( $letra ) && ( $pl != $letra ) ) continue;

			if( !isset( $palabras[$pl] ) ){
					$palabras[$pl] = array();
					$palabras[$pl]['num']=0;
			}
			$palabras[$pl]['num']++;
			$palabras[$pl][] = $word;
			$numPalabras++;
		}
		ksort($palabras);
		$this->set("palabras", $palabras);
		$this->set("numPalabras", $numPalabras);
	}


/**
 * lista las palabras en orden alfabetico, ordenadas por letra
 *
 * @param char $letra: Letra a filtrar
 * @return void
 *
 */

	public function listarPorTag( $tag_id=null ){

		if( $tag_id ){
			$this->Word->Tag->recursive = 1;
			$tag = $this->Word->Tag->findById( $tag_id );
			if( !$tag ){
				$this->redirect( array('action'=>'listar') );
			} 
			foreach ($tag['Word'] as $w) {
				$words[] = array('Word' => $w);
			}
			$this->set("cabecera", "Listado para " . $tag['Tag']['tag']);
		}else{
			$this->redirect( array('action'=>'listar') );
		}

		$palabras = $words;
		$pal = array();
		foreach ($palabras as $palabra) {
			$pal[ $palabra['Word']['word'] ] = $palabra;
		}
		ksort($pal);
		$palabras = $pal;

		$numPalabras = count( $words );
		
		$this->set("tag", $tag['Tag']['tag']);
		$this->set("palabras", $palabras);
		$this->set("numPalabras", $numPalabras);
	}



/**
 * showWord method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function showWord($id=null){
		$this->Word->recursive = 1;

		$this->Word->id = $id;
		if (!$this->Word->exists()) {
			$this->redirect( array('action'=>'listar') );
		}
		$palabra = $this->Word->read(null, $id);
		$tags = array();
		foreach ($palabra['Tag'] as $tag) {
			$tags[]=$tag['tag'];
		}
		$meanings = array();
		foreach ($palabra['Meaning'] as $meaning) {
			$meanings[]=$meaning['meaning'];
		}
		// debug( $palabra ); die;
		$this->set('palabra', $palabra);
		$this->set('tags', implode(", ", $tags) );
		$this->set('meanings', implode(", ", $meanings) );

	}

/**
 * en method
 * devuelve todas las en-palabras
 * @return void
 */
	public function en(){
		$this->Word->recursive = 1;
		$en_ord = $this->Word->find('all', array( 'conditions' => array('Word.genre'=>'en')) );
		$num_en = count( $en_ord );
		$pal = array();
		foreach ($en_ord as $ord) {
			$pal[ $ord['Word']['word'] ] = $ord;
		}
		ksort($pal);
		$en_ord = $pal;
		$this->set("en_palabras", $en_ord);
		$this->set('num_en', $num_en);

	}

/**
 * en method
 * devuelve todas las en-palabras
 * @return void
 */
	public function ett(){
		$this->Word->recursive = 1;
		
		$ett_ord = $this->Word->find('all', array( 'conditions' => array('Word.genre'=>'ett')) );
		$num_ett = count( $ett_ord );
		$pal = array();
		foreach ($ett_ord as $ord) {
			$pal[ $ord['Word']['word'] ] = $ord;
		}
		ksort($pal);
		$ett_ord = $pal;
		$this->set("ett_palabras", $ett_ord);
		$this->set('num_ett', $num_ett);
	}


	public function enEllerEtt(){
	
		$palabra = $this->getRandomWord();
		$palabra=$palabra['Word'];
		$this->set( compact('palabra') );

	}

	public function guessPlural(){
	
		$palabra = $this->getRandomWord();
		$palabra=$palabra['Word'];
		$this->set( compact('palabra') );
	}

	private function getRandomWord(){
		$this->Word->recursive=0;
		$max_id = $this->Word->find('first', array('fields'=>array('id'), 'order'=>'id DESC') );
		$max_id = $max_id['Word']['id'];
		$min_id = 1;
		$random_id = rand($min_id, $max_id);

		$palabra = null;
		$i=0;
		while (! $palabra ) {
			$random_id = rand($min_id, $max_id);
			$palabra = $this->Word->findById( $random_id );

		}
		return $palabra;
	}

	public function isAuthorized($user) {
	    // All registered users can add posts
	    if (in_array( $this->action, array('add', 'edit') )) {
	        return true;
	    }

	    return parent::isAuthorized($user);
	}


	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow('en', 'ett', 'showWord', 'listar', 'listarPorTag', 'enEllerEtt', 'guessPlural');
	}
}

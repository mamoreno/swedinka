<?php
App::uses('AppController', 'Controller');
/**
 * SimpleWords Controller
 *
 * @property SimpleWord $SimpleWord
 */
class SimpleWordsController extends AppController {


 /**
  * lista las palabras en orden alfabetico, ordenadas por letra
  *
  * @param char $letra: Letra a filtrar
  * @return void
  *
  */

    public function index( $letra = null  ){

        $words = $this->SimpleWord->find('all');

        $palabras = array();
        $numPalabras = 0;
        $n=1; //num de palabras según letra
        $l="a";
        foreach ($words as $word) {
            $w = $word[$this->modelClass]['word'];
            //primera letra de la palabra

            /*
            Hack para tomar la primera letra
            */
            $pl = substr($w, 0, 2);
            if( !in_array($pl, array('ä','ö','å', 'Ä', 'Ö', 'Å') ) ){
                $pl = substr($w, 0, 1);
            }
            /**
             * ¿Por que este hack? Esta identificando las letrs especiales como
             * dos caracteres, por lo que si solo me quedo con uno, no identifica nada
             * y muestra un ? en lugar de a con dos puntos
             * Primero pruebo si es una especial y si no lo es, pues retransformo, porque
             * si no, tendria dos letras en lugar de una, es decir, "an" en lugar de "a"
             * por haber cogido dos en el substr
             */
            if( $letra ){
                 $this->set("cabecera", "Listado para ". $letra);

            }
            if( ( $letra ) && ( $pl != $letra ) ) continue;

            if( !isset( $palabras[$pl] ) ){
                    $palabras[$pl] = array();
                    $palabras[$pl]['num']=0;
            }
            $palabras[$pl]['num']++;
            $palabras[$pl][] = $word;
            $numPalabras++;
        }
        ksort($palabras);
        $this->set("palabras", $palabras);
        $this->set("numPalabras", $numPalabras);
    }


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->SimpleWord->id = $id;
		if (!$this->SimpleWord->exists()) {
			throw new NotFoundException(__('Invalid simple word'));
		}
		$this->set('simpleWord', $this->SimpleWord->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SimpleWord->create();
			if ($this->SimpleWord->save($this->request->data)) {
				$this->Session->setFlash(__('The simple word has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The simple word could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->SimpleWord->id = $id;
		if (!$this->SimpleWord->exists()) {
			throw new NotFoundException(__('Invalid simple word'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->SimpleWord->save($this->request->data)) {
				$this->Session->setFlash(__('The simple word has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The simple word could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->SimpleWord->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->SimpleWord->id = $id;
		if (!$this->SimpleWord->exists()) {
			throw new NotFoundException(__('Invalid simple word'));
		}
		if ($this->SimpleWord->delete()) {
			$this->Session->setFlash(__('Simple word deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Simple word was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function isAuthorized($user) {
	    // All registered users can add posts
	    if (in_array( $this->action, array('add', 'edit') )) {
	        return true;
	    }

	    return parent::isAuthorized($user);
	}
}

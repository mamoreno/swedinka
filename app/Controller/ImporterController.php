<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Words Controller
 *
 */
class ImporterController extends AppController {

	var $uses=array('Word', 'Image', 'Meaning', 'Tag', 'TagsWord');

	function index(){

		$sql = "truncate table images;
		truncate table meanings;
		truncate table tags;
		truncate table tags_words;
		truncate table words;";
		$this->Word->query( $sql );

		// Find all .png in your app/webroot/img/ folder and sort the results
		$files_dir = new Folder(WWW_ROOT . 'files');
		$csv = $files_dir->find('.*\.csv');
		$lines = array();

		$gestor = fopen(WWW_ROOT . 'files/' . $csv[0], "r");
		if($gestor){
			$lineaPrimera = fgets($gestor);
			$id=1;
			while ($linea = fgets($gestor) ) {
				$a_linea = explode(",", $linea);
				$a_linea['id'] = $id++;
				$a_linea[ 'palabra' ] = $a_linea[0];
				$a_linea[ 'genero' ] = $a_linea[1];
				$a_linea[ 'articulado' ] = $a_linea[2];
				$a_linea[ 'plural' ] = $a_linea[3];
				$a_linea[ 'plural_articulado' ] = $a_linea[4];
				$a_linea[ 'grupo_forma_plural' ] = $a_linea[5];
				$a_linea[ 'significado' ] = $a_linea[6];
				$a_linea[ 'etiquetas' ] = $a_linea[7];
				$a_linea[ 'url' ] = $a_linea[8];
				$a_linea[ 'comentarios' ] = $a_linea[9];
				$a_linea[ 'grabada' ] = 0;

				$lines[] = $a_linea;
			}
		}

		$id=1;
		$fallos = 0;
		$correctas = 0;
		$palabras_guardadas = array();
		foreach ($lines as $palabra) {
			if( $palabra['significado'] === "") continue;
			if( $palabra['etiquetas'] === "") continue;
			if( $palabra['plural'] === "") continue;
			if( $palabra['plural_articulado'] === "") continue;
			if( $this->Word->findByWord( $palabra['palabra'] ) )continue;
			
			$newWord = array(
				'id' => $palabra['id']
				,'genre'=> $palabra['genero']
				,'word'=> $palabra['palabra']
				,'plural'=> $palabra['plural']
				,'articled'=> $palabra['articulado']
				,'articled_plural'=> $palabra['plural_articulado']
				,'comments'=> $palabra['comentarios']
			);
			if( $this->Word->save($newWord) ){
				$correctas++;
				$palabra['grabada']=1;
				$palabras_guardadas[]=$palabra;

			} else{
				$fallos++;
			}

		}

		//Ya tengo la palabra, ahora a meter
		//images
		$id=1;
		$fallos = 0;
		$correctas = 0;
		foreach ($palabras_guardadas as $palabra) {
			$newImage = array(
				'id' => $id
				,'word_id' => $palabra['id']
				,'url' => $palabra['url']
				);
			if( $this->Image->save($newImage) ){
				$correctas++;
				$id++;
			} else{
				$fallos++;
			}
		}

		//meanings
		$id=1;
		$fallos = 0;
		$correctas = 0;
		foreach ($palabras_guardadas as $palabra) {
			$sig = str_replace("*", ", ", $palabra['significado']);
			$newMeaning = array(
				'id' => $id
				,'word_id' => $palabra['id']
				,'meaning' => $sig
				);
			if( $this->Meaning->save($newMeaning) ){
				$correctas++;
				$id++;
			} else{
				$fallos++;
			}
		}

		//tags
		$id=1;
		$fallos = 0;
		$correctas = 0;

		foreach ($palabras_guardadas as $palabra) {
			$tags = explode("*", $palabra['etiquetas']);	
			if( !$tags ) $tags = $palabra['etiquetas'];
			foreach ($tags as $tag) {

				$t = $this->Tag->findByTag( $tag );
				if( !$t ){
					$myTag = array(
						'id' => $id++
							,'word_id' => $palabra['id']
							,'tag' => $tag
						);
					$this->Tag->save( $myTag );			
				}

			}//foreach tag
		}
		$id=1;

		//por cada palabra guardada busco las etiquetas y las relaciono
		foreach ($palabras_guardadas as $palabra) {
			$tags = explode("*", $palabra['etiquetas']);	
			if( !$tags ) $tags = $palabra['etiquetas'];
			foreach ($tags as $tag) {
				$id_p = $palabra['id'];
				$first_tag = $this->Tag->find('first', array( 'conditions' => array('Tag.tag' => $tag) ) );
				if( !$first_tag ) continue;
				$id_t = $first_tag['Tag']['id'];
				$newTagWord = array(
					'id' => $id++
					,'tag_id' => $id_t
					,'word_id' => $id_p
				);
				$this->TagsWord->save( $newTagWord );
			}
		}

		$this->set("fallos", $fallos );
		$this->set( "correctas", $correctas);

	}

}
